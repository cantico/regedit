; <?php/*

[general]
name="regedit"
version="0.6.4"
encoding="UTF-8"
mysql_character_set_database="latin1,utf8"
description="Registry editor"
description.fr="Module pour les développeurs qui permet de modifier des paramètres de configuration"
long_description.fr="README.md"
delete=1
ov_version="6.4.0"
php_version="4.1.2"
mysql_version="3.23"
author="paul de rosanbo ( paul.derosanbo@cantico.fr )"
icon="icon.png"
tags="extension,admin,dev"
;*/?>