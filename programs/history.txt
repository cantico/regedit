05/12/2016: Version 0.6.3
    - compatibility with ovidentia > 8.5

30/06/2009: Version 0.5
	- after modification of a key, we redirect on the main page

27/09/2007: Version 0.3
	- add key, delete key
	- bug fixes

06/06/2007: Version 0.2
	- key modifications for string, int, float
	