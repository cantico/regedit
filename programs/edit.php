<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
//
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once $GLOBALS['babAddonPhpPath'].'functions.php';

function regedit_edit()
{
	global $babBody;

    class temp
        { 

        function temp()
            {
            
            global $babBody;

			$this->t_key = reg_translate("Key");
			$this->t_submit = reg_translate("Record");
			$this->t_string = reg_translate("String");
			$this->t_integer = reg_translate("Integer");
			$this->t_float = reg_translate("Float");
			$this->t_delete = reg_translate("Delete");
			$this->t_add_directory = reg_translate("Add directory");
			$this->t_directory = reg_translate("Create sub-directory");
			$this->t_value = reg_translate("Value");

			$this->key = bab_rp("key");

			if ($this->key) {
				$babBody->setTitle(reg_translate("Modify a key"));
				$reg = bab_getRegistryInstance();
				$reg->changeDirectory(dirname($this->key));
				$this->value = $reg->getValue(basename($this->key));
				$this->value_type = gettype($this->value);
			} else {
				$babBody->setTitle(reg_translate("Create a key"));
				$this->directory = bab_rp("directory");
				$this->value = '';
				$this->value_type = '';
			}
        }


    }


    $babBody->addItemMenu('tree', reg_translate("Tree"), $GLOBALS['babAddonUrl'].'main');
    $babBody->addItemMenu('edit', reg_translate("Edit"), $GLOBALS['babAddonUrl'].'edit'); 
    $babBody->setCurrentItemMenu('edit');

    $tp = new temp();
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath'].'main.html', 'edit'));
}



function regedit_update($newvalue)
{
	$reg = bab_getRegistryInstance();


	if (isset($_POST['delete'])) {
		$key = bab_pp('key');
		$reg->changeDirectory(dirname($key));
		$reg->removeKey(basename($key));
	
		header('location:'.$GLOBALS['babAddonUrl'].'main');
		exit;
	}


	if (isset($_POST['new_key'])) {
		$directory = bab_pp('directory').bab_pp('new_directory');
		$reg->changeDirectory($directory);
		settype($newvalue, bab_pp('cast'));
		$reg->setKeyValue(bab_pp('new_key'), $newvalue);
		
		header('location:'.$GLOBALS['babAddonUrl'].'main');
		exit;
	} 

	$key = bab_pp('key');

	$reg->changeDirectory(dirname($key));
	$key = basename($key);
	$oldvalue = $reg->getValue($key);
	settype($newvalue, gettype($oldvalue));
	$reg->setKeyValue($key, $newvalue);
	
	header('location:'.$GLOBALS['babAddonUrl'].'main');
	exit;
}



if (isset($_POST['newvalue'])) {
	regedit_update($_POST['newvalue']);
}


regedit_edit();
