<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
//
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once $GLOBALS['babAddonPhpPath'].'functions.php';

function regedit_edit_path($path)
{
	global $babBody;

    class regedit_edit_path_temp
    { 

        function regedit_edit_path_temp($path)
        {
            
            global $babBody;

			$this->t_path = reg_translate("Path");
			$this->t_submit = reg_translate("Save");

			$babBody->setTitle(reg_translate("Move a directory"));
			$this->path = $path;
        }

    }


    $babBody->addItemMenu('tree', reg_translate("Tree"), $GLOBALS['babAddonUrl'].'main');
    $babBody->addItemMenu('move', reg_translate("Move"), $GLOBALS['babAddonUrl'].'move'); 
    $babBody->setCurrentItemMenu('edit');

    $reg = bab_getRegistryInstance();
	if (!$reg->isDirectory($path)) {
		$babBody->addError(reg_translate("The path does not exist"));
		return false;
	}
    
    $tp = new regedit_edit_path_temp($path);
	$babBody->babecho(bab_printTemplate($tp, $GLOBALS['babAddonHtmlPath'].'main.html', 'move'));
	return true;
}



function regedit_move($oldpath, $newpath)
{
	$reg = bab_getRegistryInstance();

	$reg->moveDirectory($oldpath, $newpath);
}



$newPath = bab_rp('newpath', null);
$path = bab_rp('path', null);

if (isset($newPath)) {
	regedit_move($path, $newPath);
	header('location:'.$GLOBALS['babAddonUrl'].'main');
	exit;
}


regedit_edit_path($path);
